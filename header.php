<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php bloginfo('name'); ?></title>
    <?php wp_head(); ?>
</head>

<body <?php body_class() ?>>
    <header class="site-header <?php if (is_front_page() ) { echo "header-abs"; } ?>">
        <nav class=" <?php if( is_front_page() ) { echo "transparent z-depth-0 nav-abs"; } else {echo "blue darken-4"; }?>">
            <div class="container">
            <div class="nav-wrapper">
                <a href="index.html" class="brand-logo"><?php bloginfo('name'); ?></a> <!-- add site image here -->
                <?php 
                        $args = array(
                            'theme_location' => 'primary',
                            'menu_id' => 'nav-mobile',
                            'menu_class' => 'right hide-on-med-and-down'
                        )
                    ?>
                <?php wp_nav_menu( $args ); ?>
            </div>
            </div>
        </nav>
    </header><!-- /site-header -->
    <?php if ( is_front_page() ) : ?>
        <div class="slider">  
            <ul class="slides">
            <li>
                <img src="<?php echo wp_get_attachment_url(get_theme_mod('slide-1-image')); ?>">
                <!-- random image -->
                <div class="caption center-align">
                <h3><?php echo get_theme_mod('slide-1-title'); ?></h3>
                <h5 class="light grey-text text-lighten-3"><?php echo get_theme_mod('slide-1-description'); ?></h5>
                </div>
            </li>
            <li>
            <img src="<?php echo wp_get_attachment_url(get_theme_mod('slide-2-image')); ?>">
                <!-- random image -->
                <div class="caption right-align">
                <h3><?php echo get_theme_mod('slide-2-title'); ?></h3>
                <h5 class="light grey-text text-lighten-3"><?php echo get_theme_mod('slide-2-description'); ?></h5>
                </div>
            </li>
            <li>
            <img src="<?php echo wp_get_attachment_url(get_theme_mod('slide-3-image')); ?>">
                <!-- random image -->
                <div class="caption left-align">
                <h3><?php echo get_theme_mod('slide-3-title'); ?></h3>
                <h5 class="light grey-text text-lighten-3"><?php echo get_theme_mod('slide-3-description'); ?></h5>
                </div>
            </li>
            </ul>
        </div>
    <?php endif; ?>
    <div class="container">