<?php

get_header(); 

?>

<?php

    // form init 
    $order = null;
    $gender = null;
    $country = null;
    $category = null;

    // gets current page value from the url
    $currentPage = get_query_var('paged');

    if($_SERVER['REQUEST_METHOD'] === 'GET'){

        if($_GET['order'] && !empty($_GET['order'])){
            $order = $_GET['order'];
            // array_push($query_args, 'order', $_GET[])
        }

        if($_GET['gender'] && !empty($_GET['gender'])){
            $gender = $_GET['gender'];
        }

        if($_GET['country'] && !empty($_GET['country'])){
            $country = $_GET['country'];
        }

        if($_GET['category'] && !empty($_GET['category'])){
            $category = $_GET['category'];
        }


         // setup var for WP_Query
        $query_args = array(
            'post_type' => 'icons',
            'posts_per_page' => 15,
            'paged' => $currentPage,
            'category__in' => $country,
            'tax_query' => array(
                'relation' => 'AND',

                array(
                    'taxonomy' => 'category',
                    'field' => 'slug',
                    'terms' => $gender
                ),

                array(
                    'taxonomy' => 'category',
                    'field' => 'term_id',
                    'terms' => $category
                )
            )
        );
    }
        // setup var for WP_Query
        $query_args = array(
            'post_type' => 'icons',
            'posts_per_page' => 15,
            'paged' => $currentPage,
        );
    if($country != null){
        $query_args['category__in'] = $country;
    }

    if($order != null){
        $query_args['order'] = $order;
        $query_args['orderby'] = 'title';
    }

    if($gender != null && $category != null){
        $query_args['tax_query'] = array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'category',
                    'field' => 'slug',
                    'terms' => $gender
                ),

                array(
                    'taxonomy' => 'category',
                    'field' => 'term_id',
                    'terms' => $category
                )
            );
    }

    if($gender != null && $category == null){
        $query_args['tax_query'] = array(
            'relation' => 'IN',
            array(
                'taxonomy' => 'category',
                'field' => 'slug',
                'terms' => $gender
            ),
        );
    }

    if($category != null && $gender == null){
        $query_args['tax_query'] = array(
            'relation' => 'IN',
            array(
                'taxonomy' => 'category',
                'field' => 'term_id',
                'terms' => $category
            ),
    );
    }


?>

<article class="page-profiles">
<h4>PROFILES</h4>
<div class="clearfix show_filter_wrapper">
    <button class="btn waves-effect waves-light red lighten-1 right" id="show-filter">Filter</button>
</div>
<div class="card-panel" id="filter-card">
<form method="GET">
<div class="row">
                <div class="col m2">
                    <h6>Sort by</h6>

                    <ul id="sort-by-list">
                        <li class="valign-wrapper">
                            <!-- <i class="fas fa-sort-alpha-up left"></i>Ascending</li> -->
                            <label>
                                <input class="with-gap" name="order" value="ASC" type="radio" />
                                <span>Ascending</span>
                            </label>
                        <li>
                            <!-- <i class="fas fa-sort-alpha-down left">Descending</i> -->
                            <label>
                                <input class="with-gap" name="order" value="DESC" type="radio" />
                                <span>Descending</span>
                            </label>
                        </li>
                    </ul>
                </div>
                <div class="col m8 offset-m2">
                    <h6>Gender</h6>
                    <div class="filter-section">
                        <label>
                            <input name="gender" class="with-gap" value="male" type="radio" />
                            <span>Male</span>
                        </label>
                        <label>
                            <input name="gender" class="with-gap" value="female" type="radio" />
                            <span>Female</span>
                        </label>
                    </div>

                    <h6>Country</h6>
                    <div class="filter-section">
                        <?php $countries = get_categories(array('parent' => '7')); foreach($countries as $country) {?>
                        <label>
                            <input type="checkbox" value="<?php echo $country->term_id; ?>" name="country[]" class="filled-in blue darken-4" />
                            <span><?php echo $country->name; ?></span>
                        </label>
                        <?php } ?>
                    </div>
                    <h6>Category</h6>
                    <div class="filter-section">
                    <?php $categories = get_categories(array('exclude' => '7,4,1,', 'exclude_tree' => '7,4')); foreach($categories as $category) {?>
                            <label>
                                    <input type="checkbox" value="<?php echo $category->term_id; ?>" name="category[]" class="filled-in blue darken-4" />
                                    <span><?php echo $category->name; ?></span>
                                </label>
                    <?php } ?>
                    </div>
                    
                </div>

</div>
<hr>
            <div class="center-align filter-btn-wrapper">
                <button class="btn waves-effect waves-light blue darken-4" type="submit" name="action">Filter ICONS</button>
            </div>
</form>
</div>


  <div class="card-panel">
            <ul class="collection" id="profile-list">
            <?php 

   


// get post of type icons
$iconsPosts = new WP_Query( $query_args );


if($iconsPosts->have_posts()) : 
    while ($iconsPosts->have_posts()) : $iconsPosts->the_post(); ?>

    <a class="collection-item avatar valign-wrapper" href="<?php the_permalink(); ?>">
    <!-- <li class="collection-item avatar"> -->
        <img src="<?php $img = get_field('picture', get_the_ID()); if($img) { echo $img['url']; } else { echo get_stylesheet_directory_uri() . "/img/user-thumbnail.jpg" ;} ?>" alt="" class="circle">
        <span class="title black-text"><?php the_title(); ?></span>

        <?php $tag = wp_get_post_tags(get_the_id()); 
            if($tag) : ?>
        <p class="grey-text capitalize"><?php echo $tag[0]->name ?></p>
        <?php endif; ?>
    <!-- </li> -->
</a>

    <?php endwhile; ?>
    </ul>
    <?php
      
    // next_posts_link('Next page', $iconsPosts->max_num_pages);

    echo paginate_links( array(
        'total' => $iconsPosts->max_num_pages,
        'type' => 'list'
    ));
    
    endif; 
    
    ?>
           
        </div>

    </div>
</article>
<?php
get_footer();
?>