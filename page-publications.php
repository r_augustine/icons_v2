<?php

get_header(); 

?>
<article class="page-publication">
<h4>PUBLICATIONS</h4>
<!-- <hr> -->
<?php

$query_args = array(
    'post_type' => 'books',
    'posts_per_page' => 8,
);



?>
<div class="card-panel">
    <!-- <div class="card-content"> -->
        <!-- <span class="card-title">Publications</span> -->
    <ul class="publication-list">
<?php
        // get post of type icons
        $booksPosts = new WP_Query( $query_args );

        if($booksPosts->have_posts()) : 

            while ($booksPosts->have_posts()) : $booksPosts->the_post(); 
                $id = get_the_ID();
                $img = get_field('cover', $id);
            ?>


        
            <a href=" <?php the_permalink()?>" class="publication-item"><img src="<?php echo $img['url'] ?>"></a>
        

        <?php endwhile; endif; ?>
        </ul>
        <!-- </div> -->
</div>

</article>
<?php
get_footer();
?>