<?php get_header(); ?>
<?php 
    if( have_posts()) : the_post(); 

    $img = get_field('picture');
    $lifespan = get_field('lifespan');
?>

<article>
    <div class="row">
        <div class="col s12 m3">
            <img width="100%" src="<?php if($img) { echo $img['url']; } else { echo get_stylesheet_directory_uri() . '/img/user-thumbnail.jpg'; } ?>" alt="">
            <?php $other_info = get_field('other_information'); 
                if($other_info) { 
            ?>
            <div class="other-info">
                <?php echo $other_info ?>
            </div>
                <?php } ?>
        </div>
        <div class="col s12 m9">
            <h3 class="title"><?php the_title(); ?>  <?php if($lifespan) { echo "<span class='life_span'>(".$lifespan.")</span>"; } ?></h3>
            <h6 class="sub-title grey-text"><?php echo wp_get_post_tags(get_the_id())[0]->name; ?></h6>
            <div class="content"><?php the_content(); ?></div>
            <?php $images = get_field('gallery');
                if($images) { ?>
                <section class="gallery-wrapper">
                    <h6 class="center-align white-text">GALLERY</h6>
                    <?php echo($images);
                    } ?>
                </section>
        </div>
        <div></div>
    </div>
</article>

<?php endif; ?>

<?php get_footer(); ?>