(function () {
    
    init();
    
})();


function init() {

    // var para = document.querySelector('.parallax');
    // var parallaxInstance = M.Parallax.init(para);

    var slider = document.querySelector('.slider');

    if(slider != null){
        var sliderInstance = M.Slider.init(slider, {
            indicators: false,
            height: '550'
        });
    }

    var mailchimpBtn = document.querySelector('.oceanwp-newsletter-form-wrap > form > button[type="submit"]');

    if(mailchimpBtn){
        mailchimpBtn.classList.add('btn', 'waves-effect', 'waves-light', 'red', 'lighten-1');
    }

    // get pagination from page
    var pagination = document.querySelector('.page-numbers');

    // apply materialize css to pagination
    if(pagination){
        list_items = [].slice.call(pagination.children);
        list_items.forEach(item => {
            item.classList.add('waves-effect');
        });
        pagination.classList.add('pagination', 'center-align');
    }

    // get current pagination element
    var current_pagination_page = document.querySelector('span.current');

    // apply materialize css to parent element
    if(current_pagination_page){
        current_pagination_page.parentElement.classList.add('active');
        current_pagination_page.setAttribute("style", "color: white; display: inline-block; font-size: 1.2rem;padding: 0 10px; line-height: 30px;");
    }


    var gallery_images = document.querySelectorAll('.gallery .gallery-item img');

    if(gallery_images){
        console.log(gallery_images);
       gallery_images.forEach(img => {
           img.className = "materialboxed";
        //    img.classList.add('materialboxed');
       })
    }

    // var collapsible = document.querySelector('.collapsible');
    // var collapsibleInstanse = M.Collapsible.init(collapsible);

    // const filterBtn = document.querySelector('#show-filter');
    // if(filterBtn !== null){
    //     filterBtn.addEventListener('click', toggleFilter, false);
    // }

    var lightbox = document.querySelector('.materialboxed');
    var lightboxInstance = M.Materialbox.init(lightbox);
}