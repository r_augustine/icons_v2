</div><!-- container -->
<footer class="site-footer page-footer blue darken-4">
    <div class="container">
      <div class="row">
        <div class="col s12 m4">
            <?php dynamic_sidebar('footer-1'); ?>
        </div>

        <div class="col s12 m4">
            <?php dynamic_sidebar('footer-1'); ?>
        </div>

        <div class="col s12 m4 social">
            <?php dynamic_sidebar('footer-3'); ?>
      </div>
    </div>
</div>
   
    <div class="footer-copyright">
      <div class="container">
      Copyright © 2013 NIHERST All Rights Reserved
        <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
      </div>
    </div>
    </footer>
<?php wp_footer(); ?>
</body>
</html>