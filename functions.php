<?php 



function learningWP_resources(){
    wp_enqueue_style('materialize-css', 'https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css');
    wp_enqueue_style('style', get_stylesheet_uri(), array('materialize-css'), 1.0);

    wp_enqueue_script( 'materialize-js', 'https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js', array(), 1.0, true );
    wp_enqueue_script( 'main-js', get_stylesheet_directory_uri() . '/js/main.js', array('materialize-js'), 1.0, true );
}

add_action('wp_enqueue_scripts', 'learningWP_resources');

// Navgation Menus
register_nav_menus(array(
    'primary' => __( 'Primary Menu' ),
    'footer' => __( 'Footer Menu')
));

// Add Widget Locations
function widgetInit(){

    register_sidebar( array(
        'name' => 'Footer Area 1',
        'id' => 'footer-1',
        'before_widget' => '<div class="white-text">',
        'after_widget' => '</div>',
        'before_title' => '<h5 class="white-text">',
        'after_title' => '</h5> <hr>'
    ) );

    register_sidebar( array(
        'name' => 'Footer Area 2',
        'id' => 'footer-2',
        'before_widget' => '<div class="white-text">',
        'after_widget' => '</div>',
        'before_title' => '<h5 class="white-text">',
        'after_title' => '</h5> <hr>'
    ) );

    register_sidebar( array(
        'name' => 'Footer Area 3',
        'id' => 'footer-3',
        'before_widget' => '<div class="white-text">',
        'after_widget' => '</div>',
        'before_title' => '<h5 class="white-text">',
        'after_title' => '</h5> <hr>'
    ) );
}

add_action('widgets_init', widgetInit);


// Add Slider section the admin appearance customize screen

function niherst_icons_slider_register($wp_customize){
    // Slider Section

    $wp_customize->add_section( 'header_slider' , array(
        'title'      => __('Header Slider'),
        'priority'   => 30,
    ) );

    // Slide 1 Settings

    // Title settings
    $wp_customize->add_setting( 'slide-1-title' , array(
        'default' => 'Slide 1',
        'capability'     => 'edit_theme_options',
        'type'           => 'theme_mod',
    ) );

    // Description Settings
    $wp_customize->add_setting( 'slide-1-description' , array(
        'default' => 'Lorem Ipsum is simply dummy text of the printing and type setting industry.',
        'capability'     => 'edit_theme_options',
        'type'           => 'theme_mod',
    ) );

    
    // Title Controls
    $wp_customize->add_control( new WP_Customize_Control( 
        $wp_customize, 
        'slide-1-title-control', 
        array(
            'label'      => __( 'Slide 1 Title' ),
            'section'    => 'header_slider',
            'settings'   => 'slide-1-title',
            'priority'   => 1
        )
    ));

    // Description Controls
    $wp_customize->add_control( new WP_Customize_Control( 
        $wp_customize, 
        'slide-1-description-control', 
        array(
            'label'      => __( 'Slide 1 Description' ),
            'section'    => 'header_slider',
            'settings'   => 'slide-1-description',
            'type'       => 'textarea',
            'priority'   => 2
        )
    ));

    // Image Settings
    $wp_customize->add_setting('slide-1-image');

    // Image Controls
    $wp_customize->add_control(
        new WP_Customize_Cropped_Image_Control(
            $wp_customize,
            'slide-1-image-control',
            array(
                'label'      => __( 'Slide 1 Image'),
                'section'    => 'header_slider',
                'settings'   => 'slide-1-image',
                'width'      => 1400,
                'height'     => 500,
                'priority'   => 3
            )
        ));


    // Slide 2 settings
    $wp_customize->add_setting( 'slide-2-title' , array(
        'default' => 'Slide 2',
        'capability'     => 'edit_theme_options',
        'type'           => 'theme_mod',
    ) );

    $wp_customize->add_setting( 'slide-2-description' , array(
        'default' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        'capability'     => 'edit_theme_options',
        'type'           => 'theme_mod',
    ) );

    $wp_customize->add_control( new WP_Customize_Control( 
        $wp_customize, 
        'slide-2-title-control', 
        array(
            'label'      => __( 'Slide 2 Title' ),
            'section'    => 'header_slider',
            'settings'   => 'slide-2-title',
                'priority'   => 4
        )
    ));

    $wp_customize->add_control( new WP_Customize_Control( 
        $wp_customize, 
        'slide-2-description-control', 
        array(
            'label'      => __( 'Slide 2 Description' ),
            'section'    => 'header_slider',
            'settings'   => 'slide-2-description',
            'type'       => 'textarea',
            'priority'   => 5
        )
    ));

    $wp_customize->add_setting('slide-2-image');

    $wp_customize->add_control(
        new WP_Customize_Cropped_Image_Control(
            $wp_customize,
            'slide-2-image-control',
            array(
                'label'      => __( 'Slide 2 Image'),
                'section'    => 'header_slider',
                'settings'   => 'slide-2-image',
                'width'      => 1400,
                'height'     => 500,
                'priority'   => 6
            )
        ));

    // Slide 3 settings
    $wp_customize->add_setting( 'slide-3-title' , array(
        'default' => 'Slide 3',
        'capability'     => 'edit_theme_options',
        'type'           => 'theme_mod',
    ) );

    $wp_customize->add_setting( 'slide-3-description' , array(
        'default' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        'capability'     => 'edit_theme_options',
        'type'           => 'theme_mod',
    ) );

    $wp_customize->add_control( new WP_Customize_Control( 
        $wp_customize, 
        'slider_header_3[text]', 
        array(
            'label'      => __( 'Slide 3 Title' ),
            'section'    => 'header_slider',
            'settings'   => 'slide-3-title',
                'priority'   => 7
        )
    ));

    $wp_customize->add_control( new WP_Customize_Control( 
        $wp_customize, 
        'slider_header_3[description]', 
        array(
            'label'      => __( 'Slide 3 Description' ),
            'section'    => 'header_slider',
            'settings'   => 'slide-3-description',
            'type'       => 'textarea',
            'priority'   => 8
        )
    ));

    $wp_customize->add_setting('slide-3-image');

    $wp_customize->add_control(
        new WP_Customize_Cropped_Image_Control(
            $wp_customize,
            'slide-3-image-control',
            array(
                'label'      => __( 'Slide 3 Image'),
                'section'    => 'header_slider',
                'settings'   => 'slide-3-image',
                'width'      => 1400,
                'height'     => 500,
                'priority'   => 9
            )
        ));


}

add_action('customize_register', 'niherst_icons_slider_register');
